// IMPORT ALL ACTIONS 
import * as actionTypes from './actions';


// DECLARE INITIAL STATE 
const initialState = {
  ingredients: {
    salad: 0,
    bacon: 0,
    meat: 0,
    cheese: 0
  },
  totalPrice: 4,
};

const INGREDIENT_PRICES = {
	salad: 0.5,
	cheese: 0.4,
	meat: 1.3,
	bacon: 0.7
};



const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ADD_INGREDIENT:
    return {
      // YOU NEED TO DO THIS TWICE BECAUSE THERE ARE 2 ITEMS IN STATE AND DOESNT CREATE DEEP CLONES by going into objects
      ...state,
      ingredients: {
        ...state.ingredients,
        // DYNAMICALLY OVERWRITE AN ITEM IN JS Object, CAN PASS VARIABLE OF THE ITEM YOU WANT TO USE AS A PROPERTY NAME
        // ITEM IN THE [] IS OVERWRITTEN
        [action.ingredientName]: state.ingredients[action.ingredientName] + 1
        //get number of old ingredients, add one and then assign it to and overwrite the copy we made above
      },
      totalPrice: state.totalPrice + INGREDIENT_PRICES[action.ingredientName]
    };
    case actionTypes.REMOVE_INGREDIENT:
    return {
      // YOU NEED TO DO THIS TWICE BECAUSE THERE ARE 2 ITEMS IN STATE AND DOESNT CREATE DEEP CLONES by going into objects
      ...state,
      ingredients: {
        ...state.ingredients,
        // DYNAMICALLY OVERWRITE AN ITEM IN JS Object, CAN PASS VARIABLE OF THE ITEM YOU WANT TO USE AS A PROPERTY NAME
        // ITEM IN THE [] IS OVERWRITTEN
        [action.ingredientName]: state.ingredients[action.ingredientName] - 1
        //get number of old ingredients, add one and then assign it to and overwrite the copy we made above
      },
      totalPrice: state.totalPrice - INGREDIENT_PRICES[action.ingredientName]

    };
    // esline-disable-next-line
    default:
    return state;
  };
};

export default reducer;