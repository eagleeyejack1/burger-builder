import React, { Component } from "react";

import axios from "../../axios-orders";
import Order from "../../components/Order/Order";
import withErrorHandler from "../../hoc/withErrorHandler/withErrorHandler";

class Orders extends Component {
	state = {
		orders: [],
		loading: true
	};
	componentDidMount() {
		axios
			.get("/orders.json")
			.then(res => {
				// taken your objects from firebase, converted to array and then stored in new var and then saved to state
				const fetchedOrders = [];
				for (let key in res.data) {
					fetchedOrders.push({
						// spreading the data so they have their own id
						...res.data[key],
						id: key
					});
				}
				console.log(res.data);
				this.setState({ loading: false, orders: fetchedOrders });
			})
			.catch(err => {
				this.setState({ loading: false });
			});
	}
	render() {
		return (
			<div>
				{this.state.orders.map(order => (
					<Order
						key={order.id}
						ingredients={order.ingredients}
						price={+order.price}
					/>
				))}
			</div>
		);
	}
}

export default withErrorHandler(Orders, axios);
