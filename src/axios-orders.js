import axios from "axios";

const instance = axios.create({
	baseURL: "https://burger-builder-72f97.firebaseio.com/"
});

export default instance;
